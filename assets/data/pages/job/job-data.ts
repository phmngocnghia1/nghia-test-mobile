export const jobData = [
  {
    type: 'Arbeitnehmer',
    sections: [
      {
        header: 'Drei einfache Schritte <br/> zu deinem neuen Job',
        paragraph: 'Erstellen dein Lebenslauf',
        avatar: require('~/assets/images/profile_data.svg'),
      },
      {
        header: 'Erstellen dein Lebenslauf',
        avatar: require('~/assets/images/task.svg'),
      },
      {
        header: 'Mit nur einem Klick <br/> bewerben',
        avatar: require('~/assets/images/personal_file.svg'),
      },
    ],
  },
  {
    type: 'Arbeitgeber',
    sections: [
      {
        header: 'Drei einfache Schritte zu deinem neuen Mitarbeiter',
        paragraph: 'Erstellen dein <br/> Unternehmensprofil',
        avatar: require('~/assets/images/profile_data.svg'),
      },
      {
        header: 'Erstellen dein Lebenslauf',
        avatar: require('~/assets/images/about_me.svg'),
      },
      {
        header: 'Wähle deinen <br/> neuen Mitarbeiter aus',
        avatar: require('~/assets/images/swipe_profiles.svg'),
      },
    ],
  },
  {
    type: 'Temporärbüro',
    sections: [
      {
        header:
          'Drei einfache Schritte zur <br/> Vermittlung neuer Mitarbeiter',
        paragraph: 'Erstellen dein <br/> Unternehmensprofil',
        avatar: require('~/assets/images/profile_data.svg'),
      },
      {
        header: 'Erhalte Vermittlungs-<br/>angebot von Arbeitgeber',
        avatar: require('~/assets/images/job_offers.svg'),
      },
      {
        header: 'Vermittlung nach <br/> Provision oder <br/> Stundenlohn',
        avatar: require('~/assets/images/business_deal.svg'),
      },
    ],
  },
]
