export default {
  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  ssr: false,

  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'mobile',
    htmlAttrs: {
      lang: 'en',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      /**
       * add these fonts
       */
      // Lato Italic ---
      // font-family: lato, sans-serif;
      // font-weight: 400;
      // font-style: normal;
      // Lato Bold Italic ---
      // font-family: lato, sans-serif;
      // font-weight: 700;
      // font-style: normal;
      // Lato Bold ---
      // font-family: lato, sans-serif;
      // font-weight: 700;
      // font-style: normal;
      // Lato Regular ---
      // font-family: lato, sans-serif;
      // font-weight: 400;
      // font-style: normal;
      { rel: 'stylesheet', href: 'https://use.typekit.net/wjp2ggr.css' },
    ],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ['./assets/sass/global.sass'],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    '@nuxt/typescript-build',
    // https://go.nuxtjs.dev/tailwindcss
    '@nuxtjs/tailwindcss',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [],

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},
}
