const pxToRem = (px) => (px / 16).toFixed(3) + 'rem'

const fontSize = [14, 21, 42, 75, 130].reduce((obj, px) => {
  const rem = pxToRem(px)
  obj[px] = rem
  return obj
}, {})

module.exports = {
  purge: [
    './components/**/*.{vue,js}',
    './layouts/**/*.vue',
    './pages/**/*.vue',
    './plugins/**/*.{js,ts}',
    './nuxt.config.{js,ts}',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    fontSize,
    colors: {
      green: {
        30: '#E6FFFA',
        20: '#81E6D9',
        10: '#319795',
      },
      blue: '#3182CE',
      grey: {
        80: '#F7FAFC',
        70: '#718096',
        40: '#EBF4FF',
        30: '#CBD5E0',
        20: '#4A5568',
        10: '#2D3748',
      },
      white: '#fff',
      red: '#FF0000',
    },
    backgroundColor: {
      green: {
        30: '#E6FFFA',
        20: '#81E6D9',
        10: '#319795',
      },
      blue: '#3182CE',
      grey: {
        80: '#F7FAFC',
        70: '#718096',
        40: '#EBF4FF',
        30: '#CBD5E0',
        20: '#4A5568',
        10: '#2D3748',
      },
      white: '#fff',
      red: '#FF0000',
    },
    gradients: (theme) => ({
      // linear gradients
      'green10-blue': [
        '95deg',
        theme('textColor.green.10'),
        theme('textColor.blue'),
      ],
      'grey40-green30': [
        '141deg',
        theme('textColor.grey.40'),
        theme('textColor.green.30'),
      ],
      'green30-grey40': [
        '141deg',
        theme('textColor.green.30'),
        theme('textColor.grey.40'),
      ],
    }),
    extend: {
      zIndex: {
        'negative-1': -1,
      },
      margin: {
        7.5: pxToRem(30),
        5.5: pxToRem(18),
      },
      padding: {
        4.5: pxToRem(18),
        6.5: pxToRem(26),
        5.5: pxToRem(22),
        15: pxToRem(60),
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [require('tailwindcss-plugins/gradients')],
}
